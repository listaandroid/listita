package com.example.felipevalenzuela.listitaandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import static com.example.felipevalenzuela.listitaandroid.R.id.txtUsuario;

public class MainActivity extends AppCompatActivity {

    private Button boton, botonRegistrar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        boton = (Button) findViewById(R.id.btnIngresar);
        botonRegistrar = (Button) findViewById(R.id.btnRegistro);

        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IniciarSesion();
            }
        });

        botonRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Registrarse();
            }
        });

    }

    private void IniciarSesion(){

        String usuario;
        usuario = ((EditText)findViewById(txtUsuario)).getText().toString();
        String password = ((EditText)findViewById(R.id.txtPassword)).getText().toString();
        if (usuario.equals("admin")&& password.equals("admin"))
        {
            Intent nuevoForm = new Intent(MainActivity.this, inicioActivity.class);
            startActivity(nuevoForm);
            finish();
        }
        else
        {
            Toast.makeText(getApplicationContext(),"Usuario incorrecto", Toast.LENGTH_SHORT).show();

        }
    }

    private void Registrarse() {
        Intent i = new Intent(this, RegistroActivity.class);
        startActivity(i);
        finish();

    }



}



