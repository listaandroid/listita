package com.example.felipevalenzuela.listitaandroid;

/**
 * Created by GDB on 11/26/2017.
 */

public class Usuario {

    private String username;
    private String pass;

    private String nombreUsuario;
    private String password;

    public Usuario(String password) {
        this.password = password;
    }

    public Usuario(String nombreUsuario, String password) {
        this.nombreUsuario = nombreUsuario;
        this.password = password;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}
