package com.example.felipevalenzuela.listitaandroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.content.Intent;

public class RegistroActivity extends AppCompatActivity {

    private TextView tvUserName, tvPass;
    private Button btnRegistrar, btnVolver;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        tvUserName = (TextView) findViewById(R.id.txtUsuario);
        tvPass = (TextView) findViewById(R.id.txtPassword);

        btnRegistrar = (Button) findViewById(R.id.btnRegistrarse);
        btnVolver = (Button) findViewById(R.id.btnVolverLogin);

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registrar();
            }
        });

        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                volver();
            }
        });
    }

    public void  registrar(){}
    public void volver(){
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();
    }

}
